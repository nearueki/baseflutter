const baseUrl = "http://juraganbibit.aivmedia.skom.id/api/";

class Endpoint {
  static const login = "login";
  static const logout = "logout";
  static const listdata = "listdata";
  static const adddata = "adddata";
  static const updatedata = "updatedata";
  static const deletedata = "deletedata";

}
