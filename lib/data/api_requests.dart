import 'dart:convert';
import 'package:baseflutter/data/preference.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'constants.dart';

class ApiRequests {
  Dio dio = Dio();
  var header;

  ApiRequests({@required bool withAuth}) {
    dio.interceptors.add(
        PrettyDioLogger(requestHeader: true, requestBody: true, responseBody: true, responseHeader: false, error: true, compact: true, maxWidth: 90));
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      if (withAuth) {
        debugPrint("withauth: $withAuth");
        await getToken().then((value) {
          var headers = {'content-type': 'application/json', 'Authorization': "Bearer " + value};
          // debugPrint("value: $value");
          options.headers.addAll(headers);
        });
      } else {
        var headers = {'content-type': 'application/json'};
        options.headers.addAll(headers);
      }
      return options;
    }));
  }

  /*==============================================================================
                            POST, PATCH
  ==============================================================================*/

  login(param) async {
    try {
      Response response = await dio.post("$baseUrl${Endpoint.login}", data: jsonEncode(param));
      return {'status': true, 'data': json.decode(response.toString())};
    } on DioError catch (e) {
      // debugPrint("e: ${e.response.statusCode}");
      return {'status': false, 'message': "Email atau Password Salah", 'data': e.response};
    }
  }

  addData(param) async {
    try {
      var headers = {'content-type': 'application/json'};
      Response response = await dio.post("$baseUrl${Endpoint.adddata}", data: jsonEncode(param),options: Options(
          headers: headers
      ));
      return {'status': true, 'data': json.decode(response.toString())};
    } on DioError catch (e) {
      // debugPrint("e: ${e.response.statusCode}");
      return {'status': false, 'message': checkStatusCode(e), 'data': e.response};
    }
  }

  updateData(param) async {
    try {
      Response response = await dio.post("$baseUrl${Endpoint.updatedata}", data: jsonEncode(param));
      return {'status': true, 'data': json.decode(response.toString())};
    } on DioError catch (e) {
      // debugPrint("e: ${e.response.statusCode}");
      return {'status': false, 'message': checkStatusCode(e), 'data': e.response};
    }
  }

  deleteData(param) async {
    try {
      Response response = await dio.post("$baseUrl${Endpoint.deletedata}", data: {"id": param});
      return {'status': true, 'data': json.decode(response.toString())};
    } on DioError catch (e) {
      // debugPrint("e: ${e.response.statusCode}");
      return {'status': false, 'message': checkStatusCode(e), 'data': e.response};
    }
  }


/*  ==============================================================================
                                    GET
  ==============================================================================*/
  // GET
  listdata() async {
    try {
      Response response = await dio.get(
        "$baseUrl${Endpoint.listdata}",
      );
      return {'status': true, 'data': json.decode(response.toString())};
    } on DioError catch (e) {
      return {'status': false, 'message': checkStatusCode(e), 'data': e.response};
    }
  }


  getlogout() async {
    try {
      Response response = await dio.get("$baseUrl${Endpoint.logout}");
      return {'status': true, /*'data': json.decode(response.toString())*/};
    } on DioError catch (e) {
      // debugPrint("e: ${e.response.statusCode}");
      return {'status': false, 'message': checkStatusCode(e)};
    }
  }


  /*  ==============================================================================
                                    Check Status Code
  ==============================================================================*/

  checkStatusCode(DioError e) {
    if (e.response != null) {
      if (e.response.statusCode == 500) {
        return "Terjadi masalah, silahkan coba lagi nanti";
      } else {
        return "Terjadi masalah\n[" + e.message + "]";
      }
    } else {
      return "Terjadi masalah, Silahkan coba lagi nanti.";
    }
  }
}
