import 'package:flutter/material.dart';

class EmptyWidget extends StatelessWidget {
  final String message;
  EmptyWidget({
    Key key,
    this.message
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(
            left: 16.0, right: 16.0),
        alignment: Alignment.center,
        child: Text(
          message,
        ));
  }
}
