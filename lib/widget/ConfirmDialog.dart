import 'dart:ui';
import 'package:baseflutter/utilities/tools.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class ConfirmDialog extends StatefulWidget {
  final String message;
  final Function() onOK;

  const ConfirmDialog(
      {Key key,  this.message,this.onOK })
      : super(key: key);

  @override
  _ConfirmDialog createState() => _ConfirmDialog();
}

class _ConfirmDialog extends State<ConfirmDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 0,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Container(
      height: ukuran(context, 55),
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 30,left: 30,bottom: 30,top: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Konfirmasi",
                  style: TextStyle(
                      color: Color(0xFF4F4F4F),
                      fontWeight: FontWeight.bold,
                      fontSize: ukuran(context, 7)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 25.0, bottom: 10.0),
                  child: Center(
                    child: Text(
                      widget.message,
                      style: TextStyle(
                          color: Color(0xFF4F4F4F),
                          fontWeight: FontWeight.bold,
                          fontSize: ukuran(context, 4),

                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),

              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  padding: EdgeInsets.only(bottom: 20),
                  width: ukuran(context, 30),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: Color(0xFF4F4F4F),
                    padding:
                        EdgeInsets.symmetric(vertical: 15.0, horizontal: 33.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Text( "Batal",
                                  style: TextStyle(
                                      fontSize: ukuran(context, 4.5),
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                        ),
                  ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  padding: EdgeInsets.only(bottom: 20,),
                  width: ukuran(context, 30),
                  child: RaisedButton(
                    onPressed: widget.onOK,
                    color: Colors.orange,
                    padding:
                        EdgeInsets.symmetric(vertical: 15.0, horizontal: 33.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Text( "OK",
                                  style: TextStyle(
                                      fontSize: ukuran(context, 4.5),
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                        ),
                  ),
              ],
            ),
            ),

          Positioned(
              top: -3,
              right:-3,
              child: ButtonTheme(
                minWidth: 0,
                child: MaterialButton(
                  padding: EdgeInsets.all(0),
                  onPressed: (){ Navigator.pop(context); },
                  child: Container(
                    width: ukuran(context,12),
                    height: ukuran(context,12),
                    decoration: BoxDecoration(
                        color: Color(0xFF4F4F4F), borderRadius: BorderRadius.only(topRight: Radius.circular(9),bottomLeft: Radius.circular(12))),
                    child: IconButton(icon: Icon(Icons.clear), onPressed: null,color: Colors.white,disabledColor: Colors.white,iconSize: 20,),
                  )
                ),
              )
              ),

        ],
      ),
    );
  }
}
