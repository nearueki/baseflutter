import 'package:flutter/material.dart';

class ProgressDialog extends StatefulWidget {
  @override
  _ProgressDialogState createState() => _ProgressDialogState();
}

class _ProgressDialogState extends State<ProgressDialog>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    /*_animationController =
        new AnimationController(vsync: this, duration: Duration(milliseconds: 350));
    _animationController.repeat(reverse: true);*/
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(16.0)),
        height: 191,
        child: Column(
          // mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
           
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: CircularProgressIndicator(),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    // _animationController.dispose();
    super.dispose();
  }
}
