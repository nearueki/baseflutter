import 'package:baseflutter/bloc/login_bloc.dart';
import 'package:baseflutter/utilities/textfieldstyle.dart';
import 'package:baseflutter/utilities/tools.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PageLogin extends StatefulWidget {
  @override
  _PageLoginState createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  LoginBloc bloc;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    bloc = Provider.of(context);
    return Scaffold(
      backgroundColor:Color(0xFF86C00C),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image.asset('assets/images/illustrasib.png',width: ukuran(context, 100),),
          ),
          Container(
            width: double.infinity,
            child: Center(
              child: FittedBox(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Image.asset('assets/images/logo.png',width: ukuran(context, 60),),
                      SizedBox( height: ukuran(context, 20)),
                      Container(
                          width: ukuran(context, 80),
                          child: TextFieldStyle(
                              hintText: "Email",
                              preficon: Icon(Icons.person_rounded),
                            onSaved: (val) {
                              bloc.addDataForm("email", val);
                            },
                            validator: (val) {
                              if (val.isEmpty) {
                                return 'Masukkan email';
                              }
                              return null;
                            },
                          )),
                      SizedBox( height: ukuran(context, 6)),
                      Container(
                          width: ukuran(context, 80),
                          child: TextFieldStyle(hintText: "Password",
                              preficon: Icon(Icons.vpn_key_rounded),
                              onSaved: (val) {
                                bloc.addDataForm("password", val);
                              },
                              validator: (val) {
                                if (val.isEmpty) {
                                  return 'Masukkan Password';
                                }
                                return null;
                              },
                          )),
                      SizedBox( height: ukuran(context, 10)),
                      RaisedButton(
                        onPressed: () {
                          sendLogin();
                        },
                        color: Colors.yellowAccent[400],
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        child: Container(
                          alignment: Alignment.center,
                          width: ukuran(context, 70),
                          height: ukuran(context, 15),
                          padding: EdgeInsets.all(10.0),
                          child: StreamBuilder(
                              stream: bloc.loading,
                              builder: (context, snapshot) {
                                return snapshot.data == true
                                    ? CircularProgressIndicator()
                                    : Text(
                                      "Masuk",
                                      style: TextStyle(color: Color(0xFF86C00C),fontWeight: FontWeight.bold,fontSize: ukuran(context, 4.8)),
                                    );
                            }
                          ),
                        ),
                      ),
                      SizedBox( height: ukuran(context, 20)),

                    ],
                  ),
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

  void sendLogin() async {
    _formKey.currentState.save();
    if (_formKey.currentState.validate()) {
      var respon = await bloc.sendLogin();
      if (respon['status'] == true) {
        toast(respon['data']['message'], context, color: respon['data']['status'] ? Colors.green : Colors.red);
        if (respon['data']['status'] == true) Navigator.pushReplacementNamed(context, "/home");
      } else {
        toast(respon['message'].toString(), context, color: Colors.red);
        FocusScope.of(context).requestFocus(new FocusNode());
      }
    }
  }
}
