import 'package:baseflutter/data/preference.dart';
import 'package:flutter/material.dart';

class PageSplash extends StatefulWidget {
  @override
  _PageSplashState createState() => _PageSplashState();
}

class _PageSplashState extends State<PageSplash> {
  final controllerTopic = TextEditingController();
  bool isSubscribed = false;
  static String dataName = '';
  String token = '';
  static String dataAge = '';
  var appversion;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 1500), () async {
      var login = await isLogin();
      debugPrint("islogin ${login}");
      Navigator.pushReplacementNamed(context, login == true ? "/home" : "/login");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        color: Colors.green,
        child: Stack(
          children: <Widget>[
            Align(
                alignment: Alignment.center,
                child: Image(image: AssetImage('assets/images/logo.png'),width: 200,)
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    "Juragan Bibit",
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
