
import 'package:provider/provider.dart';

import 'bloc/login_bloc.dart';

var providers = [
  ChangeNotifierProvider<LoginBloc>.value(value: LoginBloc()),
];
