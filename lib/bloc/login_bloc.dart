import 'package:baseflutter/data/api_requests.dart';
import 'package:baseflutter/data/preference.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';


class LoginBloc extends ChangeNotifier {
  BehaviorSubject _loading = new BehaviorSubject();
  Map<String, dynamic> dataForm = {};
  Stream get loading => _loading.asBroadcastStream();
  BehaviorSubject _logout = BehaviorSubject();
  Stream get logout => _logout.asBroadcastStream();

  addDataForm(vars, val) {
    dataForm[vars] = val;
  }

  sendLogin() async {
    _loading.add(true);
    var respon = await ApiRequests(withAuth: false).login(dataForm);
    debugPrint("respon ${respon.toString()}");
    if (respon['status']==true) {
      if (respon['data']!=null) {
        await setToken(respon['data']['token']);
        await setIsLogin(true);
        var login = await isLogin();
        debugPrint("islogin ${login}");
        debugPrint("respon['data']['token']: ${respon['data']['token']}");
      }
    }
    _loading.add(false);
    return respon;
  }

  getLogout() async {
    _loading.add(true);
    var respon = await ApiRequests(withAuth: true).getlogout();
    await setIsLogin(false);
    _loading.add(false);
    return respon;
  }
}
