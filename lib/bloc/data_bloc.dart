import 'package:baseflutter/data/api_requests.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';


class DataBloc extends ChangeNotifier {
  BehaviorSubject _loading = new BehaviorSubject();
  BehaviorSubject _loadingAdd = new BehaviorSubject();
  BehaviorSubject _loadingEdit = new BehaviorSubject();
  BehaviorSubject _loadingDelete = new BehaviorSubject();
  BehaviorSubject _listdata = BehaviorSubject();
  Stream get listdata => _listdata.asBroadcastStream();
  Stream get loading => _loading.asBroadcastStream();
  Stream get loadingAdd => _loadingAdd.asBroadcastStream();
  Stream get loadingEdit => _loadingEdit.asBroadcastStream();
  Stream get loadingDelete => _loadingDelete.asBroadcastStream();


  Map<String, dynamic> dataForm = {};
  Map<String, dynamic> dataFormEdit = {};

  DataForm(vars, val) {
    dataForm[vars] = val;
  }

  editDataForm(vars, val) {
    dataFormEdit[vars] = val;
  }
  


  getListData() async {
    _listdata.value = null;
    _loading.add(true);
    dynamic data = await ApiRequests(withAuth: true).listdata();
    _listdata.value = data;
    _loading.add(false);
  }

/*  addStore() async {
    _loadingAdd.add(true);
    var respon = await ApiRequests(withAuth: true).addStore(dataForm);
    _loadingAdd.add(false);
    return respon;
  }*/

/*  editStore() async {
    _loadingEdit.add(true);
    var respon = await ApiRequests(withAuth: true).editStore(dataForm);
    _loadingEdit.add(false);
    return respon;
  }*/

  deleteCategory(id)async{
    _loadingDelete.add(true);
    var respon = await ApiRequests(withAuth: true).deleteData(id);
    _loadingDelete.add(false);
    return respon;
  }

}
