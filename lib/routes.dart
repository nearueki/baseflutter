
import 'package:baseflutter/page/pg_login.dart';
import 'package:baseflutter/page/pg_splash.dart';
import 'package:baseflutter/page/pg_home.dart';

routes(context) {
  return {
    '/': (context) => PageSplash(),
    '/login': (context) => PageLogin(),
    '/home': (context) => PageHome(),
  };
}