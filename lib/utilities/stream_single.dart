import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:baseflutter/widget/empty_widget.dart';

class StreamSingle extends StatelessWidget {
  final key;
  final bloc;
  final Function(dynamic) onDataReady;
  final Function onConnectionError;
  final Function(dynamic) onEmptyData;
  final Widget loadingWidget;

  StreamSingle(
      {@required this.bloc,
      @required this.onDataReady,
      this.key,
      this.loadingWidget,
      this.onConnectionError,
      this.onEmptyData})
      : super(key: key);

  defaultEmptyWidget(c, {message}) =>
      EmptyWidget(message: message ?? "Data tidak tersedia");

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        key: key,
        stream: bloc,
        // ignore: missing_return
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          // print("snapshot: " + snapshot.toString());
          if (snapshot.hasError) {
            return Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(color: Colors.white),
                child: Row(children: [Text("HAS ERRROR")]));
          } else {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(color: Colors.white),
                    child: Row(children: [Text("Connection None")]));
                break;
              case ConnectionState.waiting:
                if (loadingWidget != null) {
                  return loadingWidget;
                }
                return Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(color: Colors.white),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [CupertinoActivityIndicator()]));
                break;
              case ConnectionState.active:
                if (snapshot.data != null) {
                  var snap = snapshot.data;

                  if (snap['status'] == true) {
                    // print('logdata ${snap['data']}');
                    if (snap['data']['success'] == true || snap['data']['success'] == 'true') {

                      if (snap['data']['data'] != null) {
                        return onDataReady(snap['data']['data']);
                      } else {
                        if (onEmptyData != null) {
                          return onEmptyData(snapshot);
                        }
                        return defaultEmptyWidget(
                          context,
                        );
                      }
                    } else
                      return defaultEmptyWidget(context,
                          message: snap['data']['message']);
                  } else
                    return defaultEmptyWidget(context,
                        message: snap['message']);
                }
                if (onConnectionError != null) {
                  return onConnectionError();
                }
                return Container(
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(color: Colors.white),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("")]));
                // children: [Text("Connection Error")]));
                break;
              case ConnectionState.done:
                if (snapshot.data != null) {
                  var snap = snapshot.data;

                  if (snap['status'] == true) {
                    if (snap['data']['success'] == true || snap['data']['success'] == 'true') {
                      dynamic _drawData = [];
                      _drawData = snap['data']['data'];
                      if (snap['data']['data'] != null) {
                        return onDataReady(snap['data']['data']);
                      } else {
                        if (onEmptyData != null) {
                          return onEmptyData(snapshot);
                        }
                        return defaultEmptyWidget(
                          context,
                        );
                      }
                    } else
                      return defaultEmptyWidget(context,
                          message: snap['data']['message']);
                  } else
                    return defaultEmptyWidget(context,
                        message: snap['message']);
                } else {
                  return defaultEmptyWidget(
                    context,
                  );
                }
                break;
            }
          }
        });
  }
}
