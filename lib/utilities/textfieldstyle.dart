import 'dart:ui';
import 'package:flutter/material.dart';
class TextFieldStyle extends StatelessWidget {
  final String hintText,valueText;
  final Function onSaved;
  final Function validator;
  final controller;
  final preficon;
  bool obscuretext  ;

  TextFieldStyle({Key key, this.hintText, this.valueText , this.onSaved, this.validator,this.controller,this.obscuretext= false,this.preficon});

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: TextFormField(
        obscureText: obscuretext,
        controller: controller,
        initialValue: valueText,
        onSaved: onSaved,
        validator: validator,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(color: Colors.black38),
          filled: true,
          contentPadding: EdgeInsets.all(15.0),
          fillColor: Colors.white,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none),
          prefixIcon: preficon,
        ),

      ),
      data: Theme.of(context)
          .copyWith(primaryColor: Color(0xFF86C00C),),
    );
  }
}
