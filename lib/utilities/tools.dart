import 'package:flutter/material.dart';
import 'package:toast/toast.dart';


double ukuran(BuildContext context, double x) {
  double lebar = MediaQuery.of(context).size.width * x / 100;
  return lebar;
}

String gambar(String namafile){
  String alamat = "assets/images/"+namafile;
  return alamat;
}

void toast(text, context, {color}) {
  Toast.show(text, context,
      duration: 3,
      // duration: Toast.LENGTH_LONG,
      gravity: Toast.BOTTOM,
      backgroundColor: color != null ? color : Colors.black);
}

